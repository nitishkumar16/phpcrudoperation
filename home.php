<?php
session_start();

$check_email  = $_SESSION['email'];
if(!isset($check_email)){
	header("location: index.php"); // Redirecting To Other Page
}
include('config/connection.php');

?>
<!doctype html>
<html>
<head>
	<title>My Dashboard</title>
	<!-- Start the link -->
	 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- End the link -->

	<!-- Start the style -->
	<!-- End the style -->
	<style>
		.center{
			text-align: center;
			padding: 20px 0px;
		}
	</style>
</head>
<body>
	<div class="container-fluid">

				<!-- Start navigation bar -->
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav mr-auto">
				      <li class="nav-item active">
				        <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">ABOUT</a>
				      </li>
				     <li class="nav-item">
				        <a class="nav-link" href="#">SERVICES</a>
				      </li>
				       <li class="nav-item">
				        <a class="nav-link" href="#">CONTACT US</a>
				      </li>
				    </ul>
				    <ul class="navbar-nav">
				    	<li class="nav-item">
				        <a class="nav-link" href="#"><?php echo $_SESSION['email'] ?></a>
				       </li>
				     <li class="nav-item">
				        <a class="nav-link" href="config/logout.php">LOGOUT</a>
				      </li>
				  </ul>
				  </div>
				</nav>
				<!-- End navigation bar -->
				<h2 class="center">Welcome in the dashboard</h2>
			</div>		
</body>
</html>
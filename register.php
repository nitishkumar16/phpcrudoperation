<?php
include('config/connection.php');

$error = '';
$success = '';
if(isset($_POST['register'])){
	$username =  isset($_POST['username']) ? $_POST['username'] : '';
	$email =     isset($_POST['email']) ? $_POST['email'] : '';
	$password =  isset($_POST['password']) ? $_POST['password'] : '';

	if(empty($username)){
		$error = "Please enter your username";
	}
	elseif(empty($email)){
		$error = "Please enter your email";
	}
	elseif(empty($password)){
		$error = "Please enter your password";
	}
	else{

		$sql = "select email from users where email='$email'";
		$query = mysqli_query($conn,$sql);
		$rows = mysqli_num_rows($query);
		if ($rows > 0) {
		$error = "Email already exist, please try another one";
		$email = '';
		} else {
			$sql = "INSERT INTO users (username, email, password) VALUES ('$username','$email','$password')";
			if ($conn->query($sql) === TRUE) {
			$success = "New record created successfully";
			$username = '';
			$email = '';
			$password = '';
			} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}

	}
	mysqli_close($conn); // Closing Connection
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Registration</title>
<!-- Add css  and other head tags -->

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- start the style for the page -->
   <style>
   	.container{
   		margin-top:50px;
   	}
   	h2{
   		text-align: center;
   	}
   	.pull-right{
   		float: right;
   	}
   </style>
  <!-- End the style for the page -->
 </head>
 <body>
 	<div class="container">
 		<div class="row">
 			<div class="com-sm-offset-3 col-sm-9">
 				<!-- Start a registration form -->
 					<h2>Please Register Here</h2>
 					<!-- Start displaying the error message -->
					<?php 
					if($error != "") {
					?>
					<div class="alert alert-warning">
					  <strong>Warning!</strong> <?php echo $error; ?>
					</div>
					<?php
					}
					?>
					<!-- End displaying the error message -->
					<!-- Start displaying the success message -->
					<?php 
					if($success != "") {
					?>
					<div class="alert alert-success">
					  <strong>Warning!</strong> <?php echo $success; ?>
					</div>
					<?php
					}
					?>
					<!-- End displaying the success message -->

				 	<form action="" method="POST">
					  <div class="form-group">
					    <label for="username">Username</label>
					    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" value="<?php if(isset($username)) { echo $username; } ?>">
					  </div>
					   <div class="form-group">
					    <label for="email">Email</label>
					    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="<?php if(isset($email)) { echo $email; } ?>">
					  </div>
					  <div class="form-group">
					    <label for="password">Password</label>
					    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php if(isset($password)) { echo $password; } ?>">
					  </div>
					  <button type="submit" name="register" class="btn btn-primary">Register</button>
					   <a href="index.php" class="btn btn-outline-primary reg_btn pull-right">Back</a>
					</form>
					<!-- End a registration form -->
				</div>
			</div>
		</div>
 </body>
 </html>
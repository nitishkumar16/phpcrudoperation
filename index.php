<?php
session_start(); // Starting Session
include('config/connection.php');

// check the login form
$error = '';
if(isset($_POST['login'])){
	// Check if email and password is empty
	if(empty($_POST['email']) || empty($_POST['password'])){
		$error = "Email and Password is required";
	}
	else{
		//Define email and password

		$email = $_POST['email'];
		$password = $_POST['password'];

		$sql = "select * from users where email='$email' AND password='$password'";
		$query = mysqli_query($conn,$sql);
		$rows = mysqli_num_rows($query);
		if ($rows > 0) {
		$_SESSION['email']=$email; // Initializing Session
		header("location: home.php"); // Redirecting To Other Page
		} else {
		$error = "Username or Password is invalid";
		}

	}
	mysqli_close($conn); // Closing Connection
}


?>

<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<!-- Add css  and other head tags -->

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- End css and other head tags -->


<!-- Start the style  -->
   <style>
   	.container{
   		margin-top:50px;
   	}
   	.reg_btn{
   		float:right;
   	}
	</style>
<!-- End the style -->
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-offset-6 col-md-6">

			<!-- Start displaying the error message -->
			<?php 
			if($error != "") {
			?>
			<div class="alert alert-warning">
			  <strong>Warning!</strong> <?php echo $error; ?>
			</div>
			<?php
			}
			?>
			<!-- End displaying the error message -->

			<!-- Start login form -->
			<form action="" method="POST">
			  <div class="form-group">
			    <label for="email">Email address</label>
			    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
			  </div>
			  <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			  <button type="submit" name="login" class="btn btn-primary">Submit</button>
			 <a href="register.php" class="btn btn-outline-primary reg_btn">Register Here</a>
			</form>
			<!-- End login form -->


		</div>
	</div>
</div>

</body>
</html>